#! /bin/sh

#set -x


EXTRA_OPTS=


if [ -c /dev/ttyUSB1 ] ; then
    EXTRA_OPTS="--device=/dev/ttyUSB1"
elif [ -c /dev/ttyUSB0 ] ; then
    EXTRA_OPTS="--device=/dev/ttyUSB0"
fi


docker run --rm --init -v jenkins-master-data:/var/lib/jenkins/ -v /var/run/docker.sock:/var/run/docker.sock \
    -e DOCKER_GID=$(cat /etc/group | grep docker | cut -d : -f 3) ${EXTRA_OPTS} \
    -p 8080:8080 -p 50000:50000 --name jenkins-master jenkins-master:latest



#docker run --rm -ti -w /project -v /home/phil/fiji/git/abi_fiji_industrialization_po966-repo:/project --device /dev/ttyUSB0 -e HOME=/project/ espressif/idf:release-v4.2
