# Jenkins CI Setup Verhaert


---


## Jenkins Master

* *GOAL: Have a single, fully configured Jenkins master container image, including all plugins and settings,
  which can be updated periodically.*
* No manual configuration necessary, besides setup pipelines.
* Custom Docker image: **jenkins-master**
* Not based on official Jenkins Docker image (jenkinsci)
* Re-use from some scripts from jenkinsci Github (https://github.com/jenkinsci/docker)
* Groovy scripts that configure Jenkins at startup **GOAL: no manual configuration to be done (except the jobs)**
* The volume mapped to */var/lib/jenkins/* must be backed up and can be re-attached after updating jenkins-master
* Custom entrypoint script copied in /usr/local/bin/entrypoint.sh
  -  Checks *DOCKER_GID* environment variable which should contain the host group id for docker, which is required to manipulate the docker daemon over */var/run/docker.sock*. The docker gid inside is adapted before launching jenkins.
  - Launches normal entrypoint /usr/local/bin/jenkins.sh as jenkins user (TODO check what happens and maybe simplify)
* Global shared groovy libraries:
  - */usr/share/jenkins/ref/libs/*
  - *log.groovy*: log and sendmail functions (support receiving list of receivers)
* References:
  - https://code-maze.com/ci-jenkins-docker/
  - https://github.com/foxylion/docker-jenkins
  - https://jenkins.io/doc/book/pipeline/shared-libraries/
  - https://automatingguy.com/2017/12/29/jenkins-pipelines-shared-libraries/
* **WARNING**: The Docker options *--rm* and *--restart* cannot be combined


### Build Image

```
$ docker build --no-cache -t jenkins-master:latest docker/
```


### Run Master Container

```
$ docker run --restart unless-stopped -d --init -p 8080:8080 -p 50000:50000 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v jenkins-master-data:/var/lib/jenkins/ \
    -e DOCKER_GID=$(cat /etc/group | grep docker | cut -d : -f 3) \
    --name jenkins-master jenkins-master:<version>
```


---


## Jenkins Slaves

* *GOAL: Have a single, fully configured Jenkins slave container image, which can be updated periodically.
  Slaves are managed like all other service containers.*
* References:
  - https://wiki.jenkins.io/display/JENKINS/Distributed+builds
  - Best practice not to maintain the slaves from Jenkins itself, but from container management (like all other services).


### Unofficial image: docker-jenkins

* https://github.com/foxylion/docker-jenkins
* Idea good, but execution misses details.

```
$ docker run -d --rm -ti -v /var/run/docker.sock:/var/run/docker.sock -v /home/jenkins \
         -e JENKINS_URL=http://10.0.183.4:8090/ -e JENKINS_USER=admin -e JENKINS_PASS=admin123 \
         -e SLAVE_LABELS="nuc001 build" -e SLAVE_EXECUTORS=2 \
         foxylion/jenkins-slave
```

* *NOTE*: the Jenkins slave needs a an anonymous volume mapped on the workspace to run jobs in a Docker container.


### Custom image: docker-jenkins-slave-node

* https://github.com/pmariman/jenkins-docker-slave-node

```
$ docker run --rm -ti -v /var/run/docker.sock:/var/run/docker.sock \
         -e SLAVE_IP="10.0.183.4" -e SLAVE_PORT="9001" -e SLAVE_USER="admin" \
         -e SLAVE_PASSWD="admin123" -e SLAVE_EXECUTORS=2 -e SLAVE_NAME="tiny-client" \
         jenkins-slave:latest
```

* *Note*: Make sure the docker insecure registry is set on the host machine (/etc/docker/deamon.json)


---


## General References

* [Jenkins Debian packages](http://pkg.jenkins.io/debian-stable/)
* [Foxylion docker-jenkins](https://github.com/foxylion/docker-jenkins/)
* [Jenkins API Client for Go](https://github.com/bndr/gojenkins/)
