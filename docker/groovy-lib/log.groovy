def info(message) {
    echo "INFO: ${message}"
}

def warning(message) {
    echo "WARNING: ${message}"
}

def error(message) {
    echo "WARNING: ${message}"
}

def email_set_failure() {
    env.EMAIL_SUBJECT = "${currentBuild.fullDisplayName} build failure"
    env.EMAIL_BODY = "Something is wrong with ${env.BUILD_URL}, artifacts are stored"
}

def sendmail_failure(receivers) {
    email_set_failure()
    mail to: "${receivers}", subject: "${env.EMAIL_SUBJECT}", body: "${env.EMAIL_BODY}"
}

def email_set_fixed() {
    env.EMAIL_SUBJECT = "${currentBuild.fullDisplayName} build fixed"
    env.EMAIL_BODY = "Build fixed ${env.BUILD_URL}"
}

def sendmail_fixed(receivers) {
    email_set_fixed()
    mail to: "${receivers}", subject: "${env.EMAIL_SUBJECT}", body: "${env.EMAIL_BODY}"
}
