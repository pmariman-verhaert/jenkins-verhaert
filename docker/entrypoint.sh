#! /bin/sh

[ -n "${DOCKER_GID}" ] && groupmod -g ${DOCKER_GID} docker

exec su -c /usr/local/bin/jenkins.sh jenkins
