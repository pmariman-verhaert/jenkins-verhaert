#!groovy

import jenkins.model.*
import hudson.security.csrf.DefaultCrumbIssuer

def jenkins = Jenkins.getInstance()

jenkins.setCrumbIssuer(new DefaultCrumbIssuer(true))

jenkins.save()
