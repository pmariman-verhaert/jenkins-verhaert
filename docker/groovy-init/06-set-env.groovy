#!groovy

import jenkins.model.*
import hudson.slaves.EnvironmentVariablesNodeProperty

def jenkins = Jenkins.getInstance()

globalNodeProperties = jenkins.getGlobalNodeProperties()
envVarsNodePropertyList = globalNodeProperties.getAll(EnvironmentVariablesNodeProperty.class)

newEnvVarsNodeProperty = null
envVars = null

if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
  newEnvVarsNodeProperty = new EnvironmentVariablesNodeProperty();
  globalNodeProperties.add(newEnvVarsNodeProperty)
  envVars = newEnvVarsNodeProperty.getEnvVars()
} else {
  envVars = envVarsNodePropertyList.get(0).getEnvVars()
}

//envVars.put("FOO", "foo")
//envVars.put("BAR", "bar")
//envVars.put("BAZ", "baz")

jenkins.save()
