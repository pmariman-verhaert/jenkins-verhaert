#!groovy

import jenkins.model.*
import jenkins.security.s2m.AdminWhitelistRule

def jenkins = Jenkins.getInstance()

jenkins.injector.getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false);

jenkins.save()
