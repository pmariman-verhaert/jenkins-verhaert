#!groovy

import jenkins.model.Jenkins
import jenkins.plugins.git.GitSCMSource
import jenkins.plugins.git.traits.BranchDiscoveryTrait
import org.jenkinsci.plugins.workflow.libs.GlobalLibraries
import org.jenkinsci.plugins.workflow.libs.LibraryConfiguration
import org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever

List libraries = [] as ArrayList

def scm = new GitSCMSource('/usr/share/jenkins/ref/lib/')

scm.traits = [new BranchDiscoveryTrait()]
def retriever = new SCMSourceRetriever(scm)

def library = new LibraryConfiguration('verhaert', retriever)
library.defaultVersion = 'master'
library.implicit = true
library.allowVersionOverride = true
library.includeInChangesets = true

libraries << library

def global_settings = Jenkins.instance.getExtensionList(GlobalLibraries.class)[0]
global_settings.libraries = libraries
global_settings.save()
