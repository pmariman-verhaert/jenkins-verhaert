#!groovy

import jenkins.model.*

def jenkins = Jenkins.getInstance()

jenkins.setNumExecutors(System.getenv('JENKINS_EXECUTORS').toInteger())

jenkins.save()
