#!groovy

import jenkins.model.JenkinsLocationConfiguration

def jenkinsLocationConfiguration = JenkinsLocationConfiguration.get()

jenkinsLocationConfiguration.setUrl('http://nuc001:9001')
jenkinsLocationConfiguration.setAdminAddress("jenkins@verhaert.com")

jenkinsLocationConfiguration.save()
