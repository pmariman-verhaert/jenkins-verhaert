#! /bin/sh

set -x

# XXX run as root
if [ $(id -u) -ne 0 ] ; then
	echo "run as root"
	exit 127
fi


# set hostname
hostnamectl set-hostname NUC001

apt-get update && apt-get upgrade -y
apt-get install -y avahi-daemon zsh apt-transport-https ca-certificates curl software-properties-common \
	python3-pip vim git zsh cmake silversearcher-ag

# install Docker

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
apt-get update
apt-get install -y docker-ce

# create users
for user in pmariman bdevos
do
	echo "checking user ${user}"
	id ${user} > /dev/null 2>&1
	if [ $? -ne 0 ] ; then
		echo "adding user"
		useradd -m -G sudo,docker ${user}
	fi
	echo "${user}:${user}123" | chpasswd
done

# install golang
GOTARFILE="go1.16.4.linux-amd64.tar.gz"
curl https://dl.google.com/go/${GOTARFILE} --output ${GOTARFILE} > /dev/null 2>&1
rm -rf /usr/local/go && tar -C /usr/local -xzf ${GOTARFILE} && rm -rf ${GOTARFILE}
export PATH=$PATH:/usr/local/go/bin
go version | grep linux > /dev/null

#install service file and local files
cp jenkins.service /etc/systemd/system/
chown 0:0 /etc/systemd/system/jenkins.service
chmod 664 /etc/systemd/system/jenkins.service
cp -r jenkins_deploy /usr/local/share/
chown -R 0:0 /usr/local/share/jenkins_deploy/
systemctl daemon-reload
systemctl enable jenkins.service
systemctl start jenkins.service
