#! /bin/sh

set -e
set -x


run_local() {
    docker build -t nginx-reverse-proxy .
    docker run --rm -ti -v /home/pmariman/fiji/git/jenkins-verhaert/nuc/nginx/share/:/usr/share/nginx/data/ \
        -p 4431:4431 nginx-reverse-proxy
}


run_compose() {
    ENV_FILE="./env.txt"

    if [ -c /dev/ttyUSB1 ] ; then
        DEVICE="/dev/ttyUSB1"
    elif [ -c /dev/ttyUSB0 ] ; then
        DEVICE="/dev/ttyUSB0"
    fi

    DOCKER_GID=$(cat /etc/group | grep docker | cut -d : -f 3)

    echo "DOCKER_GID=${DOCKER_GID}" > ${ENV_FILE}

    [ -n "${DEVICE}" ] && echo "DEVICE=${DEVICE}" >> ${ENV_FILE}

    docker-compose --env-file ${ENV_FILE} up --detach

    rm ${ENV_FILE}
}


run_compose
