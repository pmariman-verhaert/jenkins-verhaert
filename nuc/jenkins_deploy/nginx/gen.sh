
// TODO certificat config
// https://www.humankode.com/ssl/create-a-selfsigned-certificate-for-nginx-in-5-minutes

openssl req -new -newkey rsa:4096 -nodes -keyout localhost.key -out localhost.crt -config localhost.conf
openssl x509 -req -sha256 -days 365 -in localhost.crt -signkey localhost.key -out localhost.pem


